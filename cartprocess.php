<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	File to Process the items after button is pressed
*/
	$xml = new DOMDocument("1.0");
	$filename = '../../data/goods.xml';//Relative Path to the XMl file
	
	if (file_exists($filename)) //Checking if the XML file exists
	{
		$xml->load($filename);//Loading the XML file
		$items = $xml->getElementsByTagName('item');
		$nodeToRemove = array();
		
		foreach( $items as $val) 
		{ 	
			// Reading the nodes from goods.xml file
			$quantityhold = $val->getElementsByTagName("quantityhold"); 
			$quantityholdtext  = $quantityhold->item(0)->nodeValue;
			
			$quantitysold = $val->getElementsByTagName("quantitysold"); 
			$quantitysoldtext  = $quantitysold->item(0)->nodeValue;
			$quantitysold->item(0)->nodeValue = "0" ;
			
			$quantity = $val->getElementsByTagName("quantity"); 
			$quantitytext  = $quantity->item(0)->nodeValue;
			
			if((intval($quantityholdtext) <= 0 ) && (intval($quantitytext) <= 0))
				$nodeToRemove[] = $val;	
		}
		// Removing the Item whose Quantity Available is 0
		foreach($nodeToRemove as $node){ 
			$xml->documentElement->removeChild($node);
		}
		$xml->formatOutput = true;
		$xml->saveXML(); //Saving the updated goods.xml
		$xml->save($filename) or die("Error");	
		echo "Your Processing has been Successfully done";
	}
?>


