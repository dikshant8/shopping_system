/**
* Author: Dikshant Bawa
* Purpose: This file is for Assignment-2 WAD for Customer Registration
* Student ID- 4942892
* Tutorial - Monday 19:30-20:30PM
* Tutor's Name - Tarique Anwar
* Created: 13 October 2014
* Last updated: 15 October 2014
*  
*/

var xHRObject = false;
// Creating a valid XHR Object
if (window.XMLHttpRequest)
    xHRObject = new XMLHttpRequest();
else if (window.ActiveXObject)
    xHRObject = new ActiveXObject("Microsoft.XMLHTTP");

//This function is used to validate data input by the customer while registering on our system
function registerValidate(){
	var pwd = document.getElementById("pwd").value;
	var confirmpwd = document.getElementById("confirmpwd").value;
	var email = document.getElementById("email").value;
	var phone = document.getElementById("phone").value;
	var fname = document.getElementById("customerfname").value;
	var lname = document.getElementById("customerlname").value;
	if (!((fname=="") || (lname=="") || (pwd == "") ||(confirmpwd == "") || (email=="")))//Checking all fields are input
	{
		if(pwd == confirmpwd)//Comparing both Passwords
		{
			//Regular Expression for checking the Email Address
			var regexemail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(regexemail.test(email)== true)
			{
				if (phone != "")
				{
					//Regular Expression for checking the Phone number according to Australian format
					var regexphone = /^\D*0(\D*\d){9}\D*$/;
					if(regexphone.test(phone) == true)
					{
						//Making Asynchronous GET request to register.php and sending values as parameters 
						xHRObject.open("GET", "register.php?id=" + Number(new Date) +"&fname=" + fname + "&lname=" + lname + "&email=" + email + "&pwd=" + pwd, true);
						xHRObject.onreadystatechange = function()
						{
							if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
							{
								//displaying the message in the span and creating a link to go back to homepage
								document.getElementById('message').innerHTML = xHRObject.responseText;
								var link = document.createElement('a');
								link.textContent = 'Back';
								link.href = 'buyonline.htm';
								document.getElementById('message').appendChild(link);
							}
						
						}
						xHRObject.send(null);
					}
					else
						document.getElementById('message').innerHTML ="Enter a Valid Phone Number";
				}	
				else  // If Phone field is left blank
				{
					xHRObject.open("GET", "register.php?id=" + Number(new Date) +"&fname=" + fname + "&lname=" + lname + "&email=" + email + "&pwd=" + pwd, true);
					xHRObject.onreadystatechange = function()
					{
						if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
						{
							//displaying the message in the span and creating a link to go back to homepage
							document.getElementById('message').innerHTML = xHRObject.responseText;
							var link = document.createElement('a');
							link.textContent = 'Back';
							link.href = 'buyonline.htm';
							document.getElementById('message').appendChild(link);
						}			
					}
					xHRObject.send(null);
				}
			}
			else
				document.getElementById('message').innerHTML =" Enter a Valid Email Address";//Displaying the error message
		}
		else 
			document.getElementById('message').innerHTML ="Password Do Not Match";
	}
	else
		document.getElementById('message').innerHTML ="Enter all required fields";
}

// Function used by Customer to login in our system
function customerLogin(){
	var pwd = document.getElementById("pwd").value;
	var customeremail = document.getElementById("customeremail").value;
	if (!((customeremail=="") || (pwd == "")))//Checking all fields are input
	{
		
		//Making Asynchronous GET request to login.php and sending values as parameters 
		xHRObject.open("GET", "login.php?id=" + Number(new Date) +"&customeremail=" + customeremail + "&pwd=" + pwd, true);
		xHRObject.onreadystatechange = function()
		{
			if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
			{
				if ( xHRObject.responseText == "success") // If Customer Email Id and Password Match
				{
					window.open("buying.htm","_self"); //Opening buying.htm
				}
				else
					document.getElementById('loginmessage').innerHTML = "Email ID and Password do not match ";
			}			
		}
		xHRObject.send(null);
	}
	else
		document.getElementById('loginmessage').innerHTML = "Enter all required fields";
}

//setInterval(shoppingCart,3000);//Refreshing the Shopping Catalogue every 3 Seconds
//Function used to Load the Shopping Catalogue on loading buying.htm
function shoppingCart()
{
	xHRObject.open("GET","cart.php?id=" + Number(new Date),true);
	var div = document.getElementById("mainContent");
	xHRObject.onreadystatechange = function()
	{
		if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
		{	
			div.innerHTML = xHRObject.responseText; //Loading the shoping catalogue table
			setTimeout("shoppingCart()",3000);
		}			
	}
	xHRObject.send(null);
}

//Function used to Add and Remove item from the shopping cart
function AddRemoveItem(action,id)
{
	//getting the row data 
	var tabl = document.getElementById("cart");
	var tr = tabl.rows[id];
	var itemnumber= tr.childNodes[0];
	var price= tr.childNodes[3];
	var quantity= tr.childNodes[4];
	
	xHRObject.open("GET", "shoppingCart.php?action=" + action + "&itemnumber=" + itemnumber.innerHTML + "&price=" + price.innerHTML + "&quantity=" + quantity.innerHTML + "&id=" + id, true);
	xHRObject.onreadystatechange = getData;
	xHRObject.send(null); 
}

//Function to get show cart data in the buying.htm
function getData()
{   
	if ((xHRObject.readyState == 4) &&(xHRObject.status == 200))
    {   		 
		// create the new element h1
		var element = document.createElement('h1');
		element.appendChild(document.createTextNode('Shopping Cart'));
			
		// GEtting the XMl response from the server
		var serverResponse = xHRObject.responseXML;
       	var header = serverResponse.getElementsByTagName("item");
       	var spantag = document.getElementById("shop");
		var x;
		var total=0;var p; 
       	spantag.innerHTML = "";
		//Creating table of shopping cart
		x = "<table border='1'>";
		x += "<tr><td>Item Number</td><td>Quantity</td><td>Price</td><td>Remove</td></tr>";
        for (i=0; i<header.length; i++)
        {  
			var id =  header[i].getElementsByTagName("id")[0].childNodes[0].nodeValue;
			var price =  header[i].getElementsByTagName("price")[0].childNodes[0].nodeValue;
			var itemnumber =  header[i].getElementsByTagName("itemnumber")[0].childNodes[0].nodeValue;
			var quantity =  header[i].getElementsByTagName("quantity")[0].childNodes[0].nodeValue;
			//Calculating Total amount
			p=parseInt(price)*parseInt(quantity);
			total+=p;
			if(quantity=="0")
			{
				continue;
			}
			x += "<tr>"
			+ "<td>" + itemnumber + "</td>"
			+ "<td>" + quantity + "</td>"
			+ "<td>" + price + "</td>"
			+ "<td>" + "<input type=\"button\" id=\"add\" value=\"Remove One from Cart\" onclick = AddRemoveItem('Remove',"+id+") />" + "</td>" // Adding Button to remove an item
			+ "</tr>";      		
        }
		x += "</table>";
		if (header.length != 0)
		{
			//Showing the shopping cart table in span
			spantag.appendChild(element);
			spantag.innerHTML += x;
			spantag.innerHTML += "The total is   $"+total+"</br>";
			// Generating confirm and cancel button in the shopping cart
			spantag.innerHTML +='<input type="button" id="confirm" value="Confirm Purchase" onclick = confirmPurchase() />'
			spantag.innerHTML +='<input type="button" id="cancel" value="Cancel Purchase" onclick = cancelPurchase() /></br>'		
		}			
    }
}

//Function used to Confirm the purchase
function confirmPurchase()
{
	xHRObject.open("GET","confirm.php?id=" + Number(new Date),true);
	var div = document.getElementById("shop");
	xHRObject.onreadystatechange = function()
	{
		if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
		{	
			 div.innerHTML = xHRObject.responseText;//Showing the confirmation message
		}			
	}
	xHRObject.send(null);
}

//Function used to Cancel the purchase
function cancelPurchase()
{
	xHRObject.open("GET","cancel.php?id=" + Number(new Date),true);
	var div = document.getElementById("shop");
	xHRObject.onreadystatechange = function()
	{
		if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
		{	
			 div.innerHTML = xHRObject.responseText;//Showing the cancellation message
		}			
	}
	xHRObject.send(null);
}

//Logout Function for Customer
function logout()
{
	//cancelPurchase();
	xHRObject.open("GET", "logoutc.php?id=" + Number(new Date), true);
	xHRObject.onreadystatechange = function()
	{
		if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
		{
			//displaying the message in the span 
			document.getElementById('message').innerHTML = xHRObject.responseText;	
		}		
	}
	xHRObject.send(null);
}