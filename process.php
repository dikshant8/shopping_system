
<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	File to display the processing item table
*/
	$xml = new DOMDocument("1.0");
	
	$filename = '../../data/goods.xml';//Relative Path to the XMl file
	
	if (file_exists($filename)) //Checking if the XML file exists
	{
		$xml->load($filename);//Loading the XML file
		//Creating the table
		echo "<table border=\"1\" id=\"cart\"><tr><th>ItemNumber</th><th>Item Name</th><th>Price</th><th>Quantity Available</th><th>Quantity on Hold</th><th>Quantity Sold</th></tr>";
		$items= $xml->getElementsByTagName("item");
		foreach( $items as $val) 
		{ 
			// Reading the nodes from xml file
			$itemnumber = $val->getElementsByTagName("itemnumber"); 
			$itemnumbertext  = $itemnumber->item(0)->nodeValue;
				
			$itemname = $val->getElementsByTagName("itemname"); 
			$itemnametext  = $itemname->item(0)->nodeValue;
				
			$quantityhold = $val->getElementsByTagName("quantityhold"); 
			$quantityholdtext  = $quantityhold->item(0)->nodeValue;
			
			$quantitysold = $val->getElementsByTagName("quantitysold"); 
			$quantitysoldtext  = $quantitysold->item(0)->nodeValue;
				
			$price = $val->getElementsByTagName("price"); 
			$pricetext  = $price->item(0)->nodeValue;
				
			$quantity = $val->getElementsByTagName("quantity"); 
			$quantitytext  = $quantity->item(0)->nodeValue;
				
			if(intval($quantitysoldtext) <= 0)
				continue;
			else
			{
				//Creating the table for Items to be processed
				echo "<tr><td>".$itemnumbertext."</td><td>".$itemnametext."</td><td>".$pricetext."</td><td>".$quantitytext."</td><td>".$quantityholdtext."</td><td>".$quantitysoldtext."</td></tr>";
			}	
		}
		echo "</table>";	
	}
?>
