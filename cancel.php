<?php
/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	IF user press cancel this file will cancel the items present in the cart
*/
session_register("cart");
header('Content-Type: text/xml');
?>
<?php 
	$cart= $_SESSION["cart"]; 		//read the cart session into local variable
	foreach($cart as $key => $value)	//Reading the cart items
	{
		$xml = new DOMDocument("1.0");
		$filename = '../../data/goods.xml';//Relative Path to the XMl file
		$xml->load($filename);
			
		$i = $key;
		$valve= $xml->getElementsByTagName("item");
		foreach( $valve as $val) 
		{ 
			$itemno = $val->getElementsByTagName("itemnumber"); 
			$itemnotext  = $itemno->item(0)->nodeValue;
			if($i == $itemnotext)	//Updating the item in the goods.xml
			{
				$quantityhold= $val->getElementsByTagName("quantityhold");
				$quantityholdtext  = $quantityhold->item(0)->nodeValue;					
				$quantityhold->item(0)->nodeValue = "0" ;
									
				$quantity= $val->getElementsByTagName("quantity"); 
				$quantitytext  = $quantity->item(0)->nodeValue;
							
				$quantitytext = intval($quantitytext);
				$quantityholdtext = intval($quantityholdtext);
					
				$quantitytext+= $quantityholdtext;
				$quantityhold->item(0)->nodeValue = "0" ;	
				$quantity->item(0)->nodeValue = $quantitytext ;
				
			}
		}
		$xml->formatOutput = true;
		$xml->saveXML() ; //Saving the updated goods .xml
		$xml->save($filename) or die("Error");	
	}	
	echo "  Your purchase request has been cancelled. Welcome to shop next time.";
	unset($_SESSION["cart"]); //Unsetting the session variable
?>

