# README #

# Buy Online Shopping System #
Buy Online Shopping system is an online shopping System where user can register and do shopping here. The Store Manager of the Buy Online Shopping System can include items in the system and Process the sold items which the customers have bought. All the data is stored in xml files and they are updated with each transaction.

Following are the files that have been used in making this system-:

* buyonline.htm – This is the homepage for BuyOnline Shopping System which has three links i.e.  Customer Registration, Customer Login and Store Manager Login.

* register.htm- This file is used to register new customer to the system. Customer will add a unique email id with name and password. After successfully registering, Customer ID will be displayed with the Thanks Message.

* login.htm – This file is used to login the customer with valid ID and password into our system. It will redirect the user to buying.htm.

* buying.htm- This file is used to Display the Shopping Catalogue of our system. When the customer adds an item in their cart, a shopping cart will be displayed below with the items selected. Customer can also remove the items from their cart. It will also display the Amount to be pay by customer. 

* * When the Item is added in the cart it will show two buttons Confirm and Cancel. Customer can confirm their purchase or cancel their purchase here. A message will be generated after their click. This page will auto retrieve the data from cart.php every 10 seconds to update the catalogue

* logoutc.htm - This file will display the Customer Id when the user click on logout link.

* mlogin.htm – This file is used to login Store Manager in our system. Upon successful login, two new links will be displayed to the Store manager listing and processing.

* listing.htm – This file is used to enter new items in the goods.xml by the store manager.
* processing.htm - This file is used to display the items sold in a tabular format to the manager. Manager can process the items here by clicking the process button. 
* logout.htm – This file is used to show the manager id when the store manager logout from the system.
* register.php - This file is used to add the customer record in an xml customer.xml. If the file is not created earlier, it will generate the file and add the data in it. It will also check that the Email ID entered by user is unique else an error message will be shown below.
* login.php – This file is used to check that the customer is registered on our system. His id is stored in the session variable here
* shoppingCart.php – This file is used to create the shopping Cart when the user enters an item in the cart. It will check if the cart is already present then add it to it else will create a new cart. It will also update goods. xml when the user enters an item in the cart by decrementing the quantity available and increasing the quantity on hold in the xml file.
* cart.php – This file will display the Shopping catalogue from reading the data from goods. xml. Item with quantity available more than 0 will be displayed.
* logoutc.php - This file will generate the message with Customer Id from session variable and will also cancel the items available in the cart that has not been confirmed.
* logout.php – This file will generate the message with manager id in it frorn the session variable.
* mlogin.php – This file is used to check the Manager Id and Password from the manager.txt. If the manager id and password exists, Manager id will be stored in session variable and manager is logged in the system.
* confirm.php – This file is used to confirm the purchase. It will update the goods.xml file with the quantity sold and decreasing the items on hold. It will generate the message showing the confirmation with the Total Amount.
* cancel.php – This file will cancel the items in the cart and show the cancellation message. It will also update the file goods.xml to its initial state before adding the items in the cart.
* customer.js - This is the JavaScript File which contains various function s relating to customer usage. It has function registerValidate to validate customer registration data.  shoppingCart to display the catalogue . customerLogin to validate login, etc.
* manager.js - This JavaScript file contains some functions related to manager usage like itemValidate, getData , logout,etc.
* cartprocess.php – This file will process the cart by removing the items whose quantity available is less tha1.
* process.php – This file will display the table in processing.htm of items which has been sold.
* listing.php - This file will add the data in goods .xml where store manager add the item in listing.htm.
* manager.txt - This text file to store Manager Id and Password.
* customer.xml – To store the customer information.
* goods.xml – To store the items added by store manager.