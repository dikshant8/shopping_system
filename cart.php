
<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
*/
	$xml = new DOMDocument("1.0");
	
	$filename = '../../data/goods.xml';//Relative Path to the XMl file
	
	if (file_exists($filename)) //Checking if the XML file exists
	{
		$xml->load($filename);//Loading the XML file
		// Displaying the SHopping Catalogue in table format
		echo "<table border=\"1\" id=\"cart\"><tr><th>ItemNumber</th><th>Name</th><th>Description</th><th>Price</th><th>Quantity</th><th>Add</th></tr>";
		
		$items= $xml->getElementsByTagName("item");
		$j=0;
		foreach( $items as $val) 
		{ 
			// Reading the nodes from xml file
			$itemnumber = $val->getElementsByTagName("itemnumber"); 
			$itemnumbertext  = $itemnumber->item(0)->nodeValue;
				
			$itemname = $val->getElementsByTagName("itemname"); 
			$itemnametext  = $itemname->item(0)->nodeValue;
				
			$description = $val->getElementsByTagName("description"); 
			$descriptiontext  = $description->item(0)->nodeValue;
			$descriptiontext = substr($descriptiontext, 0, 20);
				
			$price = $val->getElementsByTagName("price"); 
			$pricetext  = $price->item(0)->nodeValue;
				
			$quantity = $val->getElementsByTagName("quantity"); 
			$quantitytext  = $quantity->item(0)->nodeValue;
				
			if(intval($quantitytext) <= 0) //If quantity is 0 or less it will not show it
				continue;
			else
			{
				$j++;
				//Creating the table of Shop Catalogue
				echo "<tr><td>".$itemnumbertext."</td><td>".$itemnametext."</td><td>".$descriptiontext."</td><td>".$pricetext."</td><td>".$quantitytext."</td><td><input type=\"button\" id=\"add\" value=\"Add one to Cart\" onclick = AddRemoveItem('Add',".$j.") /></td></tr>";
			}	
		}
		echo "</table>";	
	}
?>
