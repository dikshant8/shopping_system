

<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	This file will make the customer.xml if not exist or will update in the existing customer.xml
*/
	$xml = new DOMDocument("1.0");
	
	$xml->preserveWhiteSpace = false;
	$xml->formatOutput = true;//Formatting the Xml File
	
	$filename = '../../data/customer.xml';//Relative Path to the XMl file
	$fname = $_GET["fname"];
	$lname = $_GET["lname"];
	$email = $_GET["email"];
	$pwd = $_GET["pwd"];
	if (file_exists($filename)) //Checking if the XML file exists
	{
		$xml->load($filename);//Loading the XML file
		$emailfound=false;
		$value= $xml->getElementsByTagName("user");
		foreach( $value as $val) 
		{ 
			$ints = $val->getElementsByTagName("email"); 
			$intVal  = $ints->item(0)->nodeValue;
			if(strcmp($email,$intVal) == 0) // Checking if given email already exists 
			{
				$emailfound=true;
				break;
			}
		}
		if ($emailfound) 
			echo "Email Address Already Exists</br>";
		else 	//If email is unique add the User to customer.xml
		{
			
			$userid=$xml->getElementsByTagName('user')->length;
			$userid+=1; // Genrating the User Id
			
			$root = $xml->documentElement; 
			$xml->appendChild($root);
			// Creating nodes respectively and appending them to user node	
			$id   = $xml->createElement("id");
			$idText = $xml->createTextNode($userid);
			$id->appendChild($idText);
				
			$firstname = $xml->createElement("firstname");
			$fnametext = $xml->createTextNode($fname);	
			$firstname->appendChild($fnametext);
				
			$lastname = $xml->createElement("lastname");
			$lnametext = $xml->createTextNode($lname);
			$lastname->appendChild($lnametext);
				
			$emailaddress = $xml->createElement("email");
			$emailtext = $xml->createTextNode($email);
			$emailaddress->appendChild($emailtext);
				
			$password = $xml->createElement("password");
			$pwdtext = $xml->createTextNode($pwd);
			$password->appendChild($pwdtext);
			//Appending all element in user node
			$user = $xml->createElement("user");
			$user->appendChild($id);
			$user->appendChild($firstname);
			$user->appendChild($lastname);
			$user->appendChild($emailaddress);
			$user->appendChild($password);
			$root->appendChild($user);//appending user to Customers i.e. root node of xml file

//$xml->preserveWhiteSpace = false;
	//		$xml->formatOutput = true;//Formatting the Xml File
			$xml->saveXML() ;
			$xml->save($filename) or die("Error");	//Saving the XMl FIle
			
			echo " You have been successfully registered in our system. Your ID is ".$userid.".</br>";// Showing the generated User id
		}
	} 
	else // If XML file does not exist before We are creating a new XMl file
	{
		//Creating the root element for the XML file
		$root = $xml->createElement("Customers");
		$xml->appendChild($root);
		// Creating nodes respectively
		$id   = $xml->createElement("id");
		$idText = $xml->createTextNode('1');// Assigning Customer Id =1 for the first user
		$id->appendChild($idText);
		
		$firstname = $xml->createElement("firstname");
		$fnametext = $xml->createTextNode($fname);
		$firstname->appendChild($fnametext);
		
		$lastname = $xml->createElement("lastname");
		$lnametext = $xml->createTextNode($lname);
		$lastname->appendChild($lnametext);
		
		$emailaddress = $xml->createElement("email");
		$emailtext = $xml->createTextNode($email);
		$emailaddress->appendChild($emailtext);
		
		$password = $xml->createElement("password");
		$pwdtext = $xml->createTextNode($pwd);
		$password->appendChild($pwdtext);
		//Appending all nodes in user element
		$user = $xml->createElement("user");
		$user->appendChild($id);
		$user->appendChild($firstname);
		$user->appendChild($lastname);
		$user->appendChild($emailaddress);
		$user->appendChild($password);
		$root->appendChild($user);//Appending the user node to root node i.e Customers

		//$xml->formatOutput = true;
		$xml->saveXML() ;
		$xml->save($filename) or die("Error");	// Saving the XML File
		
		echo " You have been successfully registered in our system. Your ID is 1.</br>";
	}
?>
