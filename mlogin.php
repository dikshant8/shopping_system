<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	php processing for Checking the used id and password
*/
	session_start();//Starting the session
	if(isset($_GET['managerid']) && isset($_GET['pwd']))//Checking if Manager Id and Password are set
	{
		$userfound=false;
		$managerid = $_GET["managerid"];
		$pwd = $_GET["pwd"];		
			
		$handle = fopen("../../data/manager.txt", "r") or die("Unable to open file!"); //Opening the file manager.txt in read mode
		while (! feof($handle) ) //Reading from the file
		{
			$line=fgets($handle);
			$line=trim($line);
			$cos = explode(",", $line);
			if(strcmp($managerid,$cos[0]) == 0) // Comparing Managerid
			{
				if (strcmp($pwd,$cos[1]) == 0) // Comparing password
				{
					$userfound = true;
					$_SESSION["managerid"] = $managerid; // Using Session Variable to store Managerid
					break;
				}
			}
		}
		fclose ($handle);
		if($userfound) // Returning the Result
			echo "success";
		
	}
?>