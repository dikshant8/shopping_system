<?php
/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	This file will confirm the purchase that user made
*/
session_register("cart");
header('Content-Type: text/xml');
?>
<?php
	$cart= $_SESSION["cart"]; 		//read the cart session into local variable
	$total=0;
	foreach($cart as $key => $value) //Reading the cart items
	{
		$xml = new DOMDocument("1.0");
		$filename = '../../data/goods.xml';//Relative Path to the XMl file
		$xml->load($filename);
			
		$total+=$value['price']*$value['quantity'];//Calculating the total	
		$i = $key;
		$valve= $xml->getElementsByTagName("item");
		foreach( $valve as $val) 
		{ 
			$itemno = $val->getElementsByTagName("itemnumber"); 
			$itemnotext  = $itemno->item(0)->nodeValue;
			if($i == $itemnotext) //Updating the item in the goods.xml
			{
				$quantityhold= $val->getElementsByTagName("quantityhold"); 
				$quantityholdtext  = $quantityhold->item(0)->nodeValue;
											
				$quantitysold= $val->getElementsByTagName("quantitysold"); 
				$quantitysold->item(0)->nodeValue = $quantityholdtext ;
								
				$quantityhold->item(0)->nodeValue = "0" ;	
			}
		}
		$xml->formatOutput = true;
		$xml->saveXML() ;//Saving the Updated XML
		$xml->save($filename) or die("Error");	
	}	
	echo "  Your purchase has been confirmed and total amount due to pay is $".$total."</br>";
	unset($_SESSION["cart"]); //Unsetting the session
?>

