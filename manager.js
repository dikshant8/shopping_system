/**
* Author: Dikshant Bawa
* Purpose: This file is for Assignment-2 WAD for Customer Registration
* Student ID- 4942892
* Tutorial - Monday 19:30-20:30PM
* Tutor's Name - Tarique Anwar
* Created: 13 October 2014
* Last updated: 15 October 2014
*  
*/

var xHRObject = false;
// Creating a valid XHR Object
if (window.XMLHttpRequest)
    xHRObject = new XMLHttpRequest();
else if (window.ActiveXObject)
    xHRObject = new ActiveXObject("Microsoft.XMLHTTP");

// This function is used to check MAnager Id and Password matchs
function getData(){
	var pwd = document.getElementById("pwd").value;
	var managerid = document.getElementById("managerid").value;
	if (!((managerid=="") || (pwd == "")))//Checking all fields are input
	{
		
		//Making Asynchronous GET request to mlogin.php and sending values as parameters 
		xHRObject.open("GET", "mlogin.php?id=" + Number(new Date) +"&managerid=" + managerid + "&pwd=" + pwd, true);
		xHRObject.onreadystatechange = function()
		{
			if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
			{
				if ( xHRObject.responseText == "success") // If MAnager Id and Password Match
				{
					document.getElementById('loginmessage').innerHTML = "Login Successfully </br>";
					
					//Showing the links in the span in unordered lists
					document.getElementById('link').innerHTML = "<ul><li>";
					//Showing the link of listing.htm
					var link = document.createElement('a'); 
					link.textContent = 'Listing';
					link.href = 'listing.htm';
					document.getElementById('link').appendChild(link);
					
					document.getElementById('link').innerHTML+="</li><li>";
					//Showing the link of processing.htm
					var link = document.createElement('a');
					link.textContent = 'Processing';
					link.href = 'processing.htm';
					document.getElementById('link').appendChild(link);
					
					document.getElementById('link').innerHTML+="</li><li>";
					//Showing the link of logout.htm
					var link = document.createElement('a');
					link.textContent = 'Logout';
					link.href = 'logout.htm';
					document.getElementById('link').appendChild(link);
					document.getElementById('link').innerHTML+="</li></ul>";
				}
				else
					document.getElementById('loginmessage').innerHTML = "Manager Id and Password do not match ";
			}			
		}
		xHRObject.send(null);
	}
	else
		document.getElementById('loginmessage').innerHTML = "Enter all required fields";
}

//This function is used to validate the data from listing.htm to add a new Item in goods.xml
function itemValidate(){
	var itemname = document.getElementById("itemname").value;
	var price = document.getElementById("price").value;
	var quantity = document.getElementById("quantity").value;
	var description = document.getElementById("description").value;
	if (!((itemname=="") || (price=="") || (quantity == "") ||(description == "") ))//Checking all fields are input
	{
		if(!((isNaN(price)) || (isNaN(quantity))))//Checking the data type of Price and Quantity
		{
			//Making Asynchronous GET request to listing.php and sending values as parameters 
			xHRObject.open("GET", "listing.php?id=" + Number(new Date) +"&itemname=" + itemname + "&price=" + price + "&quantity=" + quantity + "&description=" + description, true);
			xHRObject.onreadystatechange = function()
			{
				if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
				{
					//displaying the message in the span 
					document.getElementById('message').innerHTML = xHRObject.responseText;	
				}		
			}
			xHRObject.send(null);
		}
		else 
			document.getElementById('message').innerHTML ="Enter a valid Quantity and Price";
	}
	else
		document.getElementById('message').innerHTML ="Enter all required fields";
}

// Function Used to Reset All fields in listing.htm
function resetItem(){
	document.getElementById("itemname").value ="";
	document.getElementById("price").value="";
	document.getElementById("quantity").value ="";
	document.getElementById("description").value="";
}

//Logout Function for Manager
function logout()
{
	xHRObject.open("GET", "logout.php?id=" + Number(new Date), true);
	xHRObject.onreadystatechange = function()
	{
		if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
		{
			//displaying the message in the span 
			document.getElementById('message').innerHTML = xHRObject.responseText;	
		}		
	}
	xHRObject.send(null);
}

//Function for Showing the items in processing.htm page for the manager to process
function managerProcessing()
{
	xHRObject.open("GET","process.php?id=" + Number(new Date),true);
	var div = document.getElementById("process");
	xHRObject.onreadystatechange = function()
	{
		if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
		{	
			 div.innerHTML = xHRObject.responseText; //Loading the Processing Items table
			 div.innerHTML +='<input type="button" id="confirm" value="Process" onclick = cartProcessing() />'//Button for Processing
		}			
	}
	xHRObject.send(null);
}

//Function for Processing the cart after the button Process is pressed
function cartProcessing()
{
	xHRObject.open("GET","cartprocess.php?id=" + Number(new Date),true);
	var div = document.getElementById("process");
	xHRObject.onreadystatechange = function()
	{
		if (xHRObject.readyState == 4 && xHRObject.status == 200)//Successful communication between XHR object and server
		{	
			 div.innerHTML = xHRObject.responseText; //Displaying the PHP file generated message
		}			
	}
	xHRObject.send(null);
}