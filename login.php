<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
*/
	session_start();
	if(isset($_GET['customeremail']) && isset($_GET['pwd']))//Checking if Email Id and Password are set
	{
		$xml = new DOMDocument("1.0");
		
		$filename = '../../data/customer.xml';//Relative Path to the XMl file
		$customeremail = $_GET["customeremail"];
		$pwd = $_GET["pwd"];

		if (file_exists($filename)) //Checking if the XML file exists
		{
			$xml->load($filename);//Loading the XML file
			$customerfound=false;
			$value= $xml->getElementsByTagName("user");
			foreach( $value as $val) 
			{ 
				$email = $val->getElementsByTagName("email"); 
				$emailtext  = $email->item(0)->nodeValue;
				if(strcmp($customeremail,$emailtext) == 0) // Checking if given email matches the email in xml file
				{
					$password = $val->getElementsByTagName("password"); 
					$passwordtext  = $password->item(0)->nodeValue;
					if(strcmp($pwd,$passwordtext) == 0)  // Checking the password
					{
						$customerfound=true;
						$id = $val->getElementsByTagName("id"); 
						$idtext  = $id->item(0)->nodeValue;
						$_SESSION["customerid"] = $idtext; // Using Session Variable to store customer id
						break;
					}
				}
			}
			if ($customerfound) 
				echo "success";
			else 	
				echo "failure";
		} 
		else
			echo"failure";
	}
?>
