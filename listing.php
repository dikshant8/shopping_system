
<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
*/
	$xml = new DOMDocument("1.0");
	$xml->preserveWhiteSpace = false;
	$xml->formatOutput = true;//Formatting the Xml File
	
	$filename = '../../data/goods.xml';//Relative Path to the XMl file
	$newname = $_GET["itemname"];
	$newprice = $_GET["price"];
	$newquantity = $_GET["quantity"];
	$newdescription = $_GET["description"];
	
	if (file_exists($filename)) //Checking if the XML file exists
	{
		$xml->load($filename);//Loading the XML file
		$itemid=$xml->getElementsByTagName('item')->length;
		$itemid+=1; // Genrating the Item Number		
		$root = $xml->documentElement; 
		$xml->appendChild($root);
		// Creating nodes respectively and appending them to item node	
		
		$itemnumber   = $xml->createElement("itemnumber");
		$itemnumberText = $xml->createTextNode($itemid);// Assigning the new item number
		$itemnumber->appendChild($itemnumberText);
		
		$itemname = $xml->createElement("itemname");
		$itemnametext = $xml->createTextNode($newname);
		$itemname->appendChild($itemnametext);
		
		$price = $xml->createElement("price");
		$pricetext = $xml->createTextNode($newprice);
		$price->appendChild($pricetext);
		
		$quantity = $xml->createElement("quantity");
		$quantitytext = $xml->createTextNode($newquantity);
		$quantity->appendChild($quantitytext);
		
		$description = $xml->createElement("description");
		$descriptiontext = $xml->createTextNode($newdescription);
		$description->appendChild($descriptiontext);
		
		$quantityhold = $xml->createElement("quantityhold"); // Adding quantity on hold and initialize it to 0
		$quantityholdtext = $xml->createTextNode(0);
		$quantityhold->appendChild($quantityholdtext);
		
		$quantitysold = $xml->createElement("quantitysold");
		$quantitysoldtext = $xml->createTextNode(0);
		$quantitysold->appendChild($quantitysoldtext);
		
		//Appending all nodes in item element
		$item = $xml->createElement("item");
		$item->appendChild($itemnumber);
		$item->appendChild($itemname);
		$item->appendChild($price);
		$item->appendChild($quantity);
		$item->appendChild($description);
		$item->appendChild($quantityhold);
		$item->appendChild($quantitysold);
		$root->appendChild($item);//Appending the item node to root node i.e Goods

		//$xml->formatOutput = true;
		$xml->saveXML() ;
		$xml->save($filename) or die("Error");	// Saving the XML File
		
		echo " You have successfully added an Item in our system. Your Item Number is ".$itemid.".</br>";
	} 
	else // If XML file does not exist before We are creating a new XMl file
	{
		//Creating the root element for the XML file
		$root = $xml->createElement("Goods");
		$xml->appendChild($root);
		// Creating nodes respectively
		$itemnumber   = $xml->createElement("itemnumber");
		$itemnumberText = $xml->createTextNode('1');// Assigning Item number =1 for the first item
		$itemnumber->appendChild($itemnumberText);
		
		$itemname = $xml->createElement("itemname");
		$itemnametext = $xml->createTextNode($newname);
		$itemname->appendChild($itemnametext);
		
		$price = $xml->createElement("price");
		$pricetext = $xml->createTextNode($newprice);
		$price->appendChild($pricetext);
		
		$quantity = $xml->createElement("quantity");
		$quantitytext = $xml->createTextNode($newquantity);
		$quantity->appendChild($quantitytext);
		
		$description = $xml->createElement("description");
		$descriptiontext = $xml->createTextNode($newdescription);
		$description->appendChild($descriptiontext);
		
		$quantityhold = $xml->createElement("quantityhold");
		$quantityholdtext = $xml->createTextNode(0);
		$quantityhold->appendChild($quantityholdtext);
		
		$quantitysold = $xml->createElement("quantitysold");
		$quantitysoldtext = $xml->createTextNode(0);
		$quantitysold->appendChild($quantitysoldtext);
		
		//Appending all nodes in item element
		$item = $xml->createElement("item");
		$item->appendChild($itemnumber);
		$item->appendChild($itemname);
		$item->appendChild($price);
		$item->appendChild($quantity);
		$item->appendChild($description);
		$item->appendChild($quantityhold);
		$item->appendChild($quantitysold);
		$root->appendChild($item);//Appending the item node to root node i.e Goods

		//$xml->formatOutput = true;
		$xml->saveXML() ;
		$xml->save($filename) or die("Error");	// Saving the XML File
		
		echo " You have successfully added an Item in our system. Your Item Number is 1.</br>";
	}
?>
