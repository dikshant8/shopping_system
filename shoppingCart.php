<?php
/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	File for generating the Shopping Cart
*/
session_register("cart");
header('Content-Type: text/xml');
?>
<?php 
	//Getting the data from Get action
	$itemnumber = $_GET["itemnumber"];
	$quantity = $_GET["quantity"];
    $action = $_GET["action"];
	$price = $_GET["price"];
	$id = $_GET["id"];
	
	//if the session cart exists before
	if ($_SESSION["cart"] != "") 					
	{
		$cart = $_SESSION["cart"]; 					//read the cart session into local variable
		if ($action == "Add") 						//If the action is add
		{
			if (!isset($cart[$itemnumber]))		//Check if the item already exist in cart
			{  
				$quantity = 1; 							
				$value = array(); 					
				$value["quantity"] = $quantity;
				$value["id"] = $id;
				$value["price"] = $price;
				$cart[$itemnumber] = $value;
				$_SESSION["cart"] = $cart;  		// save the  cart to session variable 
				
				$xml = new DOMDocument("1.0");
				$filename = '../../data/goods.xml';//Relative Path to the XMl file

				$xml->load($filename);
				$valve= $xml->getElementsByTagName("item");
				foreach( $valve as $val) 
				{ 
					$itemno = $val->getElementsByTagName("itemnumber"); 
					$itemnotext  = $itemno->item(0)->nodeValue;
					if(strcmp($itemnumber,$itemnotext) == 0) // Checking the item in the goods.xml
					{
						//Incrementing the quantityhold and decrementing the quantity in goods.xml
						$qty= $val->getElementsByTagName("quantity"); 
						$qtytext  = $qty->item(0)->nodeValue;
							
						$qtytext = intval($qtytext);
						$qtytext--;
						$qty->item(0)->nodeValue = $qtytext ;
									
						$quantityhold= $val->getElementsByTagName("quantityhold"); 
						$quantityholdtext  = $quantityhold->item(0)->nodeValue;
						
						$quantityholdtext = intval($quantityholdtext);
						$quantityholdtext++;
						$quantityhold->item(0)->nodeValue = $quantityholdtext ;
							
					}
				}
				// save the updated document
				$xml->formatOutput = true;
				$xml->saveXML() ;
				$xml->save($filename) or die("Error");			
				echo (toXml($cart));   				// send XML form of CART to client
			}
			else	//If the item is already exist in cart we increment its quantity
			{
				$xml = new DOMDocument("1.0");
				$filename = '../../data/goods.xml';//Relative Path to the XMl file

				$xml->load($filename);
				$valve= $xml->getElementsByTagName("item");
				foreach( $valve as $val) 
				{ 
					$itemno = $val->getElementsByTagName("itemnumber"); 
					$itemnotext  = $itemno->item(0)->nodeValue;
					if(strcmp($itemnumber,$itemnotext) == 0) // Checking if the quantity is available for that item in goods.xml
					{
						$qty= $val->getElementsByTagName("quantity"); 
						$qtytext  = $qty->item(0)->nodeValue;
							
						$qtytext = intval($qtytext);
						$value = $cart[$itemnumber];
						if($qtytext <=0)	//If not we dont add anything to cart as item is no more available
						{
							$value["quantity"] = $value["quantity"];
							
							echo (toXml($cart));
						}
						else		//We add one more quantity to to cart for that particular item
						{
							$value["quantity"] = $value["quantity"] + 1;			
							$value["price"] = $price ;
							$cart[$itemnumber] = $value;
							$_SESSION["cart"] = $cart;
										
							$xml = new DOMDocument("1.0");
							$filename = '../../data/goods.xml';//Relative Path to the XMl file

							$xml->load($filename);
							$valve= $xml->getElementsByTagName("item");
							foreach( $valve as $val) 
							{ 
								$itemno = $val->getElementsByTagName("itemnumber"); 
								$itemnotext  = $itemno->item(0)->nodeValue;
								if(strcmp($itemnumber,$itemnotext) == 0) // Checking the item in goods.xml
								{
									//Increasing the quanitity on hold and decreasing the available quantity in goods.xml
									$qty= $val->getElementsByTagName("quantity"); 
									$qtytext  = $qty->item(0)->nodeValue;
											
									$qtytext = intval($qtytext);
									$qtytext--;
									$qty->item(0)->nodeValue = $qtytext ;
													
									$quantityhold= $val->getElementsByTagName("quantityhold"); 
									$quantityholdtext  = $quantityhold->item(0)->nodeValue;
											
									$quantityholdtext = intval($quantityholdtext);
									$quantityholdtext++;
									$quantityhold->item(0)->nodeValue = $quantityholdtext ;
											
								}
							}
							// save the updated document
							$xml->formatOutput = true;
							$xml->saveXML() ;
							$xml->save($filename) or die("Error");	
							echo (toXml($cart));
						}	
					}
				}			
			}
		}
		else 			//If the action is remove
		{
			$value = $cart[$itemnumber];
			$value["quantity"] = $value["quantity"] - 1; //Decreasing the quanitity
			$value["price"] = $price ;
			$cart[$itemnumber] = $value;
			$_SESSION["cart"] = $cart;
		
			$xml = new DOMDocument("1.0");
			$filename = '../../data/goods.xml';//Relative Path to the XMl file

			$xml->load($filename);
			$valve= $xml->getElementsByTagName("item");
			foreach( $valve as $val) 
			{ 
				$itemno = $val->getElementsByTagName("itemnumber"); 
				$itemnotext  = $itemno->item(0)->nodeValue;
				if(strcmp($itemnumber,$itemnotext) == 0) // Checking the item in goods.xml
				{
					//Increasing the quantity available and decreasing the quantity on hold in goods.xml
					$qty= $val->getElementsByTagName("quantity"); 
					$qtytext  = $qty->item(0)->nodeValue;
							
					$qtytext = intval($qtytext);
					$qtytext++;
					$qty->item(0)->nodeValue = $qtytext ;
									
					$quantityhold= $val->getElementsByTagName("quantityhold"); 
					$quantityholdtext  = $quantityhold->item(0)->nodeValue;
							
					$quantityholdtext = intval($quantityholdtext);
					$quantityholdtext--;
					$quantityhold->item(0)->nodeValue = $quantityholdtext ;
							
				}
			}
			// save the updated document
			$xml->formatOutput = true;
			$xml->saveXML() ;
			$xml->save($filename) or die("Error");			
			echo (toXml($cart));
		}
	}
	elseif ($action == "Add")			// Creating a new Cart session if Cart doesnot exist
	{
		$value = array();
		$value["quantity"] = "1";
		$value["price"] = $price;
		$value["id"] = $id;
		$cart = array();
		$cart[$itemnumber] = $value;
		$_SESSION["cart"] = $cart;
		$xml = new DOMDocument("1.0");
		$filename = '../../data/goods.xml';//Relative Path to the XMl file

		$xml->load($filename);
		$valve= $xml->getElementsByTagName("item");
			foreach( $valve as $val) 
			{ 
				$itemno = $val->getElementsByTagName("itemnumber"); 
				$itemnotext  = $itemno->item(0)->nodeValue;
				if(strcmp($itemnumber,$itemnotext) == 0) 
				{
					//Incrementing the quantityhold and decrementing the quantity in goods.xml
					$qty= $val->getElementsByTagName("quantity"); 
					$qtytext  = $qty->item(0)->nodeValue;
					
					$qtytext = intval($qtytext);
					$qtytext--;
					$qty->item(0)->nodeValue = $qtytext ;
							
					$quantityhold= $val->getElementsByTagName("quantityhold"); 
					$quantityholdtext  = $quantityhold->item(0)->nodeValue;
					
					$quantityholdtext = intval($quantityholdtext);
					$quantityholdtext++;
					$quantityhold->item(0)->nodeValue = $quantityholdtext ;
					
				}
			}
		// save the updated document
		$xml->formatOutput = true;
		$xml->saveXML() ;
		$xml->save($filename) or die("Error");	
		
		echo (toXml($cart));
	}

    function toXml($shop_cart)	//Creating the XMl to store the Items add in the cart
    {   
		$doc = new DomDocument('1.0');
        $cart = $doc->createElement('cart');
        $cart = $doc->appendChild($cart);
		
        foreach ($shop_cart as $Item => $ItemName)
        { 
			//to create <item></item>
			$thing = $doc->createElement('item');
          	$thing = $cart->appendChild($thing);
			
			//This is to create <itemnumber></itemnumber>
          	$itemnumber = $doc->createElement('itemnumber'); 
          	$itemnumber = $thing->appendChild($itemnumber);
			//and this to append inside <itemnumber>$value</itemnumber>
          	$value = $doc->createTextNode($Item);
          	$value = $itemnumber->appendChild($value);
			
          	$quantity = $doc->createElement('quantity');
          	$quantity = $thing->appendChild($quantity);
          	$value2 = $doc->createTextNode($ItemName["quantity"]);
          	$value2 = $quantity->appendChild($value2);
			
			$price = $doc->createElement('price');
          	$price = $thing->appendChild($price);
          	$value3 = $doc->createTextNode($ItemName["price"]);
          	$value3 = $price->appendChild($value3);
			
			$idElement = $doc->createElement('id');
          	$idElement = $thing->appendChild($idElement);
          	$value4 = $doc->createTextNode($ItemName["id"]);
          	$value4 = $idElement->appendChild($value4);
			
        }
		
        $strXml = $doc->saveXML(); 
        return $strXml; //Returning the generated XML
    }
?>

